Rack Demo
=========

Rack app and Rack middlewares.

# Usage

```bash
bundle install
RACK_ENV=production bundle exec rackup

# with debug prints
RACK_ENV=development bundle exec rackup
```

Send request.

```bash
curl http://localhost:9292
curl -X POST http://localhost:9292 -d "nownabe"
```
