# frozen_string_literal: true

$LOAD_PATH.unshift(File.expand_path("lib", __dir__))

require "hello"
require "add_suffix_middleware"
require "append_thanks_middleware"

use AddSuffixMiddleware, "can"
use AppendThanksMiddleware
run Hello.new
