# frozen_string_literal: true

require "debuggable"

class AppendThanksMiddleware
  include Debuggable

  attr_reader :app

  def initialize(app)
    @app = app
  end

  def call(env)
    debug(env) { _call(env) }
  end

  private

  def _call(env)
    status, headers, body = app.call(env)
    [status, headers, [*body[0...-1], "#{body.last}\nThanks"]]
  end
end
