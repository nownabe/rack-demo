# frozen_string_literal: true

require "rack"
require "debuggable"

class AddSuffixMiddleware
  include Debuggable

  attr_reader :app
  attr_reader :suffix

  def initialize(app, suffix)
    @app = app
    @suffix = suffix
  end

  def call(env)
    debug(env) { _call(env) }
  end

  private

  def _call(env)
    if env["REQUEST_METHOD"] == "POST"
      input = env[Rack::RACK_INPUT].read
      input << suffix
      env[Rack::RACK_INPUT] = StringIO.new(input)
      env["CONTENT_LENGTH"] = input.length
    end
    app.call(env)
  end
end
