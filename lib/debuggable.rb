# frozen_string_literal: true

module Debuggable
  def debug(env)
    print_env(env, "Before")
    res = yield
    print_env(env, "After", res)
    res
  end

  private

  def print_env(env, prefix, res = nil)
    if ENV["RACK_ENV"] == "development"
      env[Rack::RACK_INPUT].rewind

      puts
      puts "⭐ #{prefix} #{self.class} ⭐"
      # pp env
      puts "Request Body: #{env[Rack::RACK_INPUT].read}"
      puts "Response: #{res}"
      puts "⭐⭐⭐"
      env[Rack::RACK_INPUT].rewind
    end
  end
end
