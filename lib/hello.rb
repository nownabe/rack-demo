# frozen_string_literal: true

require "rack"
require "debuggable"

class Hello
  include Debuggable

  attr_reader :greeting

  def initialize(greeting: "Hello")
    @greeting = greeting
  end

  def call(env)
    debug(env) { _call(env) }
  end

  private

  def _call(env)
    req = Rack::Request.new(env)
    body = req.post? ? "#{greeting} #{req.body.read}" : greeting
    [
      200,
      { "Content-Type" => "text/plan" },
      [body]
    ]
  end
end
